//
//  GameScene.swift
//  b
//
//  Created by Shivani Awate on 3/30/17.
//  Copyright © 2017 shivani. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate{
    var ball:SKSpriteNode!
    var paddle:SKSpriteNode!
    var scoreLabel: SKLabelNode! // creating a score label
    var score:Int = 0 {
        didSet {
            scoreLabel.text = "Score:\(score)"    //update the score label
        }
    }
    
    override func didMove(to view: SKView) {
        ball = self.childNode(withName: "Ball") as! SKSpriteNode
        paddle = self.childNode(withName: "Paddle") as! SKSpriteNode
        scoreLabel = self.childNode(withName: "Score") as! SKLabelNode
        
        ball.physicsBody?.applyImpulse(CGVector(dx: 50, dy: 50))
        
        let border = SKPhysicsBody(edgeLoopFrom: (view.scene?.frame)!)
        border.friction = 0
        self.physicsBody = border
        
        self.physicsWorld.contactDelegate = self
        
    }
    //moving the paddle by touch
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let touchLocation = touch.location(in: self)
            paddle.position.x = touchLocation.x
        
        }
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let touchLocation = touch.location(in: self)
            paddle.position.x = touchLocation.x
            
        }

    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        let bodyAName = contact.bodyA.node?.name
        let bodyBName = contact.bodyB.node?.name
        
        //either bodyA or bodyB collision occurs delete it
        if bodyAName == "Ball" && bodyBName == "Brick" || bodyAName == "Brick" && bodyBName == "Ball" {
            if bodyAName == "Brick" {
                contact.bodyA.node?.removeFromParent()
                score += 1
                
            } else if bodyBName == "Brick" {
                contact.bodyB.node?.removeFromParent()
                score += 1
                
            }
        }
    }
    //win and loose logic
    override func update(_ currentTime: TimeInterval) {
        if (score == 9) {
            scoreLabel.text = "Hurray You WIN!!!"
            self.view?.isPaused = true
        }
        
        if (ball.position.y < paddle.position.y) {
            scoreLabel.text = "Sorry You Lost!!!"
            self.view?.isPaused = true
        }
    }
    

    
}
